from pymem import Pymem
from pymem.ptypes import RemotePointer
import keyboard
pm = Pymem("HoloCure.exe")
def GetRemotePointerAddr(base, offsets):
    remote_pointer = RemotePointer(pm.process_handle, base)
    for offset in offsets:
        if offset != offsets[-1]:
            remote_pointer = RemotePointer(pm.process_handle, remote_pointer.value + offset)
        else:
            return remote_pointer.value + offset
def WriteNumber(value : float):
    addr_ptrgold = GetRemotePointerAddr(pm.base_address + 0x314EE48, offsets=[0x1B0, 0xE70])
    pm.write_double(addr_ptrgold, value)
def ReadDoubleNumber():
    pymem = Pymem("{}.exe".format("HoloCure"))
    rem_ptrgold = GetRemotePointerAddr(pymem.base_address + 0x314EE48, offsets=[0x1B0, 0xE70])
    return pymem.read_double(rem_ptrgold)

if __name__ == "__main__":
    while True:
        if keyboard.is_pressed("F"):
            print(ReadDoubleNumber())
            exit(443)
        elif keyboard.is_pressed("ALT+Y"):
            WriteNumber(float(500000))
            print(ReadDoubleNumber())
            exit(5455)